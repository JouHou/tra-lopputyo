/*
 * This program reads a .txt -file word by word, stores all of its unique words in a hash table and reports what was the
 * number of total and unique words in text. After that, it reads a second .txt -file word by word and after each word
 * checks if the word exists in the hash table, if it doesn't, that word is marked as "different", in other words, that
 * word doesn't exist in the first file. The program prints 50 first words that a) exist in the second file and b) don't
 * exist in the first file. Also, for benchmarking the performance of the hashing function, number of hash collisions
 * when hashing the first text is reported. Individual functions are explained in detail below.
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define TABLE_SIZE 99991
#define YES 1
#define NO 0
#define MAX_BUF_SZ 256

int read_file (FILE*);
int insert_string (char*);

int cmp_file (FILE*);
int search_string (char*);

unsigned int hash (char const *);


char array[TABLE_SIZE][MAX_BUF_SZ]={0};
char tempC;
char tempStr[MAX_BUF_SZ];

FILE *fptr;

int collisions=0;
int uniqWrds=0;
int dffrntWrds=1;
int wordOrNot=YES;
int wordCount=0;

int main (int argc, char *argv[]) {

    if (argc != 3) {
        printf("Wrong number of arguments, try again");
        return 0;
    }

    if ((fptr=fopen(argv[1], "r")) == NULL) {
        printf("Cannot open the file. Check the file name and try again.");
        return 0;
    }

    read_file(fptr);
    fclose(fptr);

    if ((fptr=fopen(argv[2], "r")) == NULL) {
        printf("Cannot open the file. Check the file name and try again.");
        return 0;
    }

    printf("%s contained %d unique words and total number of words was %d.\n", argv[1], uniqWrds, wordCount);
    printf("Total number of hash collisions with the base file was %d.\n\n", collisions);
    printf("The first 50 words occurring in %s and NOT in %s are:\n\n", argv[2], argv[1]);

    while (cmp_file(fptr) != 1) {
    }
    fclose(fptr);

    return 0;
}

/*
 * FUNCTION FOR HASHING THE WORDS. WORKS BY USING THE ASCII VALUES OF THE STRING TO BE HASHED. MULTIPLIES BY A PRIME
 * NUMBER 43 TO GENERATE MORE (?) DIVERSE HASH VALUES. MODULUS OF THE HASH VALUE IS USED TO PREVENT THE PROGRAM FROM
 * GOING OUTSIDE ARRAY BOUNDARIES.
 */
unsigned int hash (char const *tempStr ) {
    int i=0;
    unsigned int hashValue=0;

    for (i=0;tempStr[i]!='\0';i++) {
        hashValue=43*hashValue+tempStr[i];
    }
    return hashValue%TABLE_SIZE;
}

/*
 * FUNCTION FOR READING THE WORDS OF THE FIRST FILE. READS IT CHARACTER BY CHARACTER AND EVERY TIME A NON-ALPHABET
 * CHARACTER OR '\'' IS ENCOUNTERED, THE STRING IS TERMINATED WITH '\0' AND THE WORD IS PASSED TO insert_string
 * -FUNCTION. THE wordOrNot-FLAG IS USED TO PREVENT USELESS CALLS TO insert_string -FUNCTION. IF insert_string RETURNS
 * -1, THE HASH TABLE IS FULL AND PROGRAM IS TERMINATED. IN THE CURRENT VERSION, HASH TABLE SIZE IS FIXED.
 */
int read_file (FILE *fptr) {
    int i=0;

    while ((tempC=fgetc(fptr)) != EOF) {
        if (isalpha(tempC) || (tempC == '\'') ) {
            tempStr[i]=tolower(tempC);
            i++;
            wordOrNot=YES;
        } else if ( wordOrNot == YES){
            tempStr[i]='\0';
            if (insert_string(tempStr) == -1) {
                printf("Table full, make it bigger.");
                return 0;
            }
            i=0;
            wordCount++;
            wordOrNot=NO;
        } else {
            wordOrNot=NO;
        }
    }
    return 1;
}

/*
 * FUNCTION FOR SAVING THE WORDS OF THE FIRST FILE TO THE HASH TABLE. USES LINEAR PROBING TO DEAL WITH COLLISIONS.
 * ALSO KEEPS TRACK OF HASH COLLISIONS. IF THE LINEAR PROBING CAUSES THE HASH VALUE TO INCREASE OVER THE TABLE SIZE, IT
 * GETS RESET TO ZERO. INDEX VARIABLE IS USED TO PREVENT INFINITE LOOPS; IF IT REACHES THE TABLE SIZE, IT MEANS THAT NO
 * EMPTY TABLE SLOTS WERE FOUND AND -1 IS RETURNED.
 */
int insert_string (char* tempStr) {
    int hashValue=hash(tempStr);
    int i=0;

    while (i<TABLE_SIZE) {
        if (strlen(array[hashValue]) == 0) {
            strcpy(array[hashValue],tempStr);
            uniqWrds++;
            return 1;
        } else if (strcmp(tempStr,array[hashValue]) == 0) {
            return 1;
        } else {
            hashValue++;
            i++;
            collisions++;
            if (hashValue>=TABLE_SIZE) {
                hashValue=0;
            }
        }
    }
    return -1;
}

/*
 * FUNCTION FOR READING THE WORDS OF THE SECOND FILE. THE FUNCTIONALITY DIFFERS ONLY A LITTLE FROM THE read_file
 * FUNCTION. READS THE SECOND FILE CHARACTER BY CHARACTER AND EVERY TIME A NON-ALPHABET CHARACTER OR '\'' IS
 * ENCOUNTERED, THE STRING IS TERMINATED WITH '\0' AND THE WORD IS PASSED TO search_string -FUNCTION. THE wordOrNot-FLAG
 * IS USED TO PREVENT USELESS CALLS TO search_string -FUNCTION.
 *
 */
int cmp_file (FILE *fptr) {
    int i=0;

    while ((tempC=fgetc(fptr)) != EOF && dffrntWrds<=50) {
        if (isalpha(tempC) || (tempC == '\'') ) {
            tempStr[i]=tolower(tempC);
            i++;
            wordOrNot=YES;
        } else if ( wordOrNot == YES){
            tempStr[i]='\0';
            if (search_string(tempStr) == 1) {
                printf("%d. %s\n", dffrntWrds-1, tempStr);
                i=0;
                wordOrNot=NO;
            } else {
                i=0;
                wordOrNot=NO;
            }
        } else {
            wordOrNot=NO;
        }
    }
    return 1;
}

/*
 * FUNCTION FOR COMPARING THE WORDS OF THE SECOND FILE TO THE WORDS OF THE FIRST FILE. ALSO KEEPS TRACK OF WORDS NOT
 * FOUND IN THE FIRST FILE. IF AN EMPTY SLOT IS ENCOUNTERED, THE WORD IS REPORTED AS DIFFERENT AND SAVED TO THE HASH
 * TABLE. INDEX VARIABLE IS USED TO PREVENT INFINITE LOOPS; IF IT REACHES THE TABLE SIZE, IT MEANS THAT NO
 * EMPTY TABLE SLOTS WERE FOUND AND -1 IS RETURNED.
 */
int search_string (char* tempStr) {
    int hashValue=hash(tempStr);
    int i=0;

    while (i<TABLE_SIZE) {
        if (strlen(array[hashValue]) == 0) {
            dffrntWrds++;
            strcpy(array[hashValue],tempStr);
            return 1;
        } else if (strcmp(tempStr,array[hashValue]) == 0) {
            return -1;
        } else {
            hashValue++;
            i++;
            if (hashValue>=TABLE_SIZE) {
                hashValue=0;
            }
        }
    }
    return -1;
}