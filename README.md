A school project for University of Oulu's Data Structures and Algorithms course. Reads one text file and saves its unique words into a hash table. Then it reads another text file and reports 50 first words occurring in the second book, but not in the first one. Also reports number of unique words in the first text file and number of hash collisions during the first text file processing.

Usage:
- gcc -o myprog
- ./myprog firsttext secondtext
